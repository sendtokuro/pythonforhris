#import tool to load excel
from openpyxl import load_workbook

variableDict={"name":"Peter","gender":"male","age":"30","salary":60000}
print("name:{}".format(variableDict["name"]))
print("gender:{}".format(variableDict["gender"]))
print("age:{}".format(variableDict["age"]))
print("salary:{}".format(variableDict["salary"]))

#update item
variableDict["salary"]=120000
print("updated salary:{}".format(variableDict["salary"]))

variableEmptyDict={}
print("variableEmptyDict:{}".format(variableEmptyDict))

#assign new item
variableEmptyDict["title"]="HRIS Analyst"
print("title in variableEmptyDict:{}".format(variableEmptyDict["title"]))

print("for more tutorial about python please go to : https://www.tutorialspoint.com/python/python_variable_types.htm")