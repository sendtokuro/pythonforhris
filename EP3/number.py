#import tool to load excel
from openpyxl import load_workbook

#assign variable with value
variableNumber=9
print("variableNumber:{}".format(variableNumber))
variableNumberPlusOne=variableNumber+1
print("variableNumber+1:{}".format(variableNumberPlusOne))
variableNumberTimesThree=variableNumber*3
print("variableNumber*3:{}".format(variableNumberTimesThree))
variableNumberOverThree=variableNumber/3
print("variableNumber/3:{}".format(variableNumberOverThree))

variableNumberOverTwo=variableNumber/2
print("variableNumber/2 for python 3:{}".format(variableNumberOverTwo))
print("variableNumber/2 for python 2:{}".format(int(variableNumberOverTwo)))

variableNumberModuloTwo=variableNumber%2
print("variableNumber%2:{}".format(variableNumberModuloTwo))