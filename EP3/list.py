#import tool to load excel
from openpyxl import load_workbook

variableListOfNumber=[1,2,3]
print("variableListOfNumber:{}".format(variableListOfNumber))
variableListOfNumber.append(4)
print("variableListOfNumber appended with 4:{}".format(variableListOfNumber))
variableListOfNumber[0]=5
print("variableListOfNumber updated first time with 5:{}".format(variableListOfNumber))

variableListOfString=["hello","world"]
print("variableListOfString:{}".format(variableListOfString))
print("variableListOfString concatenation:{}".format(variableListOfString[0]+variableListOfString[1]))