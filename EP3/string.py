#import tool to load excel
from openpyxl import load_workbook

variableString="hello world"
print("variableString:{}".format(variableString))
variableStringSubstring=variableString[:5]
print("variableString substring with first 5 letters:{}".format(variableStringSubstring))
variableStringAddedString=variableString+" again"
print("variableString added string:{}".format(variableStringAddedString))
