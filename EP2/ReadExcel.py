#import tool to load excel
from openpyxl import load_workbook

#open excel file
workbook=load_workbook("ExcelExample.xlsx")

#open sheet1
sheet1=workbook["Sheet1"]

#read data in cell D2
cell=sheet1["D2"]
print("Salary at D2:{}".format(cell.value))

#read data in cell D2
cell2=sheet1.cell(2,4)
print("Salary at row 2, column 4:{}".format(cell2.value))

#modify data in cell D2
sheet1.cell(2,4,60000)

#write excel file
workbook.save("ExcelExample.xlsx")







